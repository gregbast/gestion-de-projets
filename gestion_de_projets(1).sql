-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 17, 2021 at 04:31 PM
-- Server version: 8.0.27-0ubuntu0.20.04.1
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gestion_de_projets`
--

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `ID` int NOT NULL,
  `nom` varchar(200) NOT NULL,
  `adresse` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`ID`, `nom`, `adresse`) VALUES
(1, 'daniel', 'marseille');

-- --------------------------------------------------------

--
-- Table structure for table `dev`
--

CREATE TABLE `dev` (
  `ID` int NOT NULL,
  `first_name` varchar(200) NOT NULL,
  `last_name` varchar(200) NOT NULL,
  `level` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `dev`
--

INSERT INTO `dev` (`ID`, `first_name`, `last_name`, `level`) VALUES
(1, '', '', ''),
(2, 'greg', 'bastard', 'nulle'),
(3, 'tony', 'maseriole', 'moyen'),
(4, 'eeee', 'eeeee', 'eeeeee'),
(5, 'arsene', 'ntibushitse', 'moyen'),
(6, 'hussam', 'ahtash', 'moyen'),
(7, 'eeeee', 'eeeee', 'eeeee'),
(8, 'ddddd', 'dddddd', 'dddddd'),
(9, 'hava', 'bakrieva', 'junior');

-- --------------------------------------------------------

--
-- Table structure for table `projets`
--

CREATE TABLE `projets` (
  `ID` int NOT NULL,
  `nom_projet` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `description_projet` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `projets`
--

INSERT INTO `projets` (`ID`, `nom_projet`, `description_projet`) VALUES
(7, 'site web du psg', 'allez l\'om!! allez l\'om!!allez l\'om!!allez l\'om!!allez l\'om!!allez l\'om!!allez l\'om!!allez l\'om!!allez l\'om!!allez l\'om!!allez l\'om!!allez l\'om!!allez l\'om!!allez l\'om!!allez l\'om!!allez l\'om!!allez l\'om!!allez l\'om!!allez l\'om!!allez l\'om!!allez l\'om!!allez l\'om!!allez l\'om!!allez l\'om!!allez l\'om!!allez l\'om!!allez l\'om!!allez l\'om!!allez l\'om!!allez l\'om!!allez l\'om!!allez l\'om!!allez l\'om!!allez l\'om!!allez l\'om!!allez l\'om!!allez l\'om!!allez l\'om!!allez l\'om!!allez l\'om!!allez l\'om!!allez l\'om!!allez l\'om!!'),
(8, 'Application mobile tisseo', 'lorem\r\nf type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.'),
(11, 'refonte du site web m2k', 'industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in');

-- --------------------------------------------------------

--
-- Table structure for table `projet_dev`
--

CREATE TABLE `projet_dev` (
  `ID_dev` int NOT NULL,
  `ID_projet` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `dev`
--
ALTER TABLE `dev`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `projets`
--
ALTER TABLE `projets`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `ID` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `dev`
--
ALTER TABLE `dev`
  MODIFY `ID` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `projets`
--
ALTER TABLE `projets`
  MODIFY `ID` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
