<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>dev_list</title>
</head>

<body>
    <header>
        <p>Gestion de projets</p>
        <p>Simplon.co</p>
    </header>
    <div class="subhead">
        <img src="logo_simplon.png" alt="logo de simplon">
        <h1>Gestion de projets</h1>

    </div>
    <h4>List des developpeurs</h4>
    <table>
        <tr class="projet_head">
            <th class="dev">Nom</th>
            <th class="dev">prenom</th>
            <th class="dev">Niveau</th>

            <th class="action"></th>

        </tr>
        <?php

        require './connect.php';
        $dev = $mysqli->query('SELECT * FROM dev');

        foreach ($dev as $dev_list) { ?>
            <tr>
                <td class="projet1">
                    <?php echo $dev_list['last_name'] ?>
                </td>

                <td class="projet1">
                    <?php echo $dev_list['first_name'] ?>
                </td>
                <td class="projet1">
                    <?php echo $dev_list['level'] ?>
                </td>
                <td>

                </td>
            </tr>
        <?php } ?>
    </table>
    <a class="back" href="index.php">
        << retour</a>
            <footer>
                <div class="foo">
                    <h4>@Simplon.co</h4>
                </div>
            </footer>

</body>

</html>