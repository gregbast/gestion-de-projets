<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>page</title>
</head>

<body>
    <header>
        <p>Gestion de projets</p>
        <p>Simplon.co</p>
    </header>

    <div class="subhead">
        <img src="logo_simplon.png" alt="logo de simplon">
        <h1>Gestion de projets</h1>
    </div>
    <?php

    require './connect.php';
    if (isset($_GET['id'])) {
        $id = (int) $_GET['id'];
        $res = $mysqli->query("SELECT * FROM projets WHERE ID = " . $id);
        $row = $res->fetch_assoc();

        if (isset($row)) {
    ?>
            <div class="pagedesc">
                <h2> <?php echo $row['nom_projet']; ?></h2>
                <p> <?php echo $row['description_projet']; ?></p>
            </div>
    <?php }
    } ?>
    <a class="back" href="index.php">
        << retour</a>
            <footer>
                <div class="foo">
                    <h4>@Simplon.co</h4>
                </div>
            </footer>
</body>

</html>