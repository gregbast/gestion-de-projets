         <?php require 'connect.php'; ?>

         <!DOCTYPE html>
         <html lang="en">

         <head>
             <meta charset="UTF-8">
             <meta http-equiv="X-UA-Compatible" content="IE=edge">
             <meta name="viewport" content="width=device-width, initial-scale=1.0">
             <link rel="stylesheet" href="style.css">
             <title>gestion de projets</title>
         </head>


         <body>
             <header>
                 <p>Gestion de projets</p>
                 <p>Simplon.co</p>
             </header>
             <div class="subhead">
                 <img src="logo_simplon.png" alt="logo de simplon">
                 <h1>Gestion de projets</h1>
             </div>
             <div class="container">
                 <div class="bouton">
                     <div class="ajout">
                         <a class="btn1" href="dev.php">dev liste.</a>
                     </div>
                     <div class="ajout">
                         <a class="btn1" href="add_dev.php"> Nouveau dev</a>
                     </div>
                     <div class="ajout">
                         <a class="btn1" href="clients.php">nouveau client.</a>
                     </div>
                     <div class="ajout">
                         <a class="btn1" href="client_page.php">liste des clients.</a>
                     </div>
                     <div class="ajout">
                         <a class="btn1" href="projets_add.php">Ajouter un projet.</a>
                     </div>
                 </div>
                 <table>
                     <tr class="projet_head">
                         <th class="projet">projet</th>
                         <th class="action"></th>

                     </tr>
                     <?php


                        $proj = $mysqli->query('SELECT * FROM projets');

                        foreach ($proj as $projet) { ?>
                         <tr>
                             <td class="projet1">
                                 <?php echo $projet['nom_projet'] ?>
                             </td>

                             <td>

                                 <div class="act">
                                     <a class=" btn" href="projets_page.php?id=<?php echo $projet['ID']; ?>">voir</a>
                                     <div class="divide"></div>
                                     <a class=" btn" href="projets_edit.php?id=<?php echo $projet['ID']; ?>">Modifier</a>
                                     <div class="divide"></div>
                                     <a class="btn " href="delete.php?id=<?php echo $projet['ID']; ?>">Supprimer</a>
                                 </div>
                             </td>
                         </tr>
                     <?php } ?>
                 </table>
             </div>
             <footer>
                 <div class="foo">
                     <h4>@Simplon.co</h4>
                 </div>
             </footer>

         </body>

         </html>